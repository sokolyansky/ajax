var labelForm = document.querySelectorAll('label');
var signIn = document.querySelector('button[data-role="json-request-form-submit"]');
var signOut = document.querySelector('button[data-role="json-request-user-profile-logout"]');
var preloader = document.querySelector('span[data-role="json-request-preloader"]');
var error = document.querySelector('div[data-role="json-request-error"]');
var email = document.getElementsByName('email')[0];
var password = document.getElementsByName('password')[0];
var userImg = document.querySelector('img[data-role="json-request-user-profile-avatar"]');
var userFullName = document.querySelector('span[data-role="json-request-user-profile-fullname"]');
var userCountry = document.querySelector('span[data-role="json-request-user-profile-country"]');
var userHobbies = document.querySelector('span[data-role="json-request-user-profile-hobbies"]');

function setProfile(obj) {
  if(obj) {
    userImg.src = obj.userpic;
    userFullName.innerHTML = '<br/>' + 'Full name : ' + obj.name + ' ' + obj.lastname;
    userCountry.innerHTML = '<br/>' + 'Country : ' + obj.country;
    userHobbies.innerHTML = '<br/>' + 'Hobbies : ' + obj.hobbies + '<br/>';
    return;
  }
  userImg.src = '';
  userFullName.innerHTML = '';
  userCountry.innerHTML = '';
  userHobbies.innerHTML = '';
  clearForm();
}
function showForm(flag, response) {

  if(!flag) {
    signIn.style.display = 'none';
    signOut.style.display = 'none';
    labelForm[0].style.display = 'none';
    labelForm[1].style.display = 'none';
    error.innerHTML = '';
    return;
  }
  signIn.style.display = 'inline-block';
  signOut.style.display = 'inline-block';
  labelForm[0].style.display = 'inline';
  labelForm[1].style.display = 'inline';
  if(response) {
    error.innerHTML = response.error.code + response.error.message;
    error.style.color = 'red';
  }
}

function clearForm() {
  email.value = '';
  password.value = '';
  error.innerHTML = '';
}

signIn.onclick = function () {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', 'http://netology-hj-ajax.herokuapp.com/homework/login_json');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onloadstart = function () {
    showForm(false);
    preloader.innerHTML = 'Loading ...';
  };
  xhr.onloadend = function () {
    preloader.innerHTML = '';
  };

  xhr.addEventListener('load', function () {
    var res = JSON.parse(xhr.responseText);

    if(xhr.status === 200) {
      showForm(false);
      signOut.style.display = 'inline-block';
      setProfile(res);
    } else {
      showForm(true, res);
      /*error.innerHTML = res.error.code + res.error.message;
      error.style.color = 'red';*/
    }
  });

  var body = 'email=' + encodeURIComponent(email.value) + '&password=' + encodeURIComponent(password.value);
  xhr.send(body);

  return false;
};

signOut.onclick = function () {
  setProfile();
  showForm(true);
};
